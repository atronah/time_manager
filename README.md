Purposes
--------
- [ ] human readable storing format
- [ ] simplest start\stop time counting by CLI
- [ ] store ticket identity and title
- [ ] automatic combine similar countings (related to the same tasks)
- [ ] start noname counting and ability to describe it later
- [ ] access to counting data from everywhere (Dropbox sync maybe)
- [ ] support separating by clients
- [ ] show summary for the day, grouped by clients and tasks


CLI drafts
----------
- `tm` - shows current counting
- `tm s[tart] client [ticket_id] [title]  [-d description] [-t tags]` - starts new task (and finishes active previous task)
- `tm f[inish]` - finish current tasks
- `tm c[ontinue]` - restarts last counting
- `to l[og]` - shows log of tasks
- `tm m[anage]` - shows manage menu with list of 16 last countings
- `tm r[eport]` - shows reports menu